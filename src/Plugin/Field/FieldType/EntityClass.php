<?php

namespace Drupal\entity_class\Plugin\Field\FieldType;

use Drupal\Core\Field\Plugin\Field\FieldType\StringItem;

/**
 * Provides a Entity Class field type.
 *
 * @FieldType(
 *   id = "entity_class",
 *   label = @Translation("Entity class"),
 *   default_formatter = "entity_class",
 *   default_widget = "entity_class",
 * )
 */

class EntityClass extends StringItem {

}
