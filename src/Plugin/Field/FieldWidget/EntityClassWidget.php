<?php

namespace Drupal\entity_class\Plugin\Field\FieldWidget;

use Drupal\Core\Field\Plugin\Field\FieldWidget\StringTextfieldWidget;

/**
 * Plugin implementation of the 'entity_class' widget.
 *
 * @FieldWidget(
 *   id = "entity_class",
 *   label = @Translation("Textfield"),
 *   field_types = {
 *     "entity_class"
 *   }
 * )
 */
class EntityClassWidget extends StringTextfieldWidget {
}
