Entity Class

The module allows to add HTML classes to any rendered entity via UI.

## Installation

Install the module as usual. It does not have any dependencies.

## Usage

Create a field of "Entity class" type to any entity bundle.
When creating / editing an entity of type a needed class in the field text input.
Make sure the class is added to the entity wrapping HTML element.
